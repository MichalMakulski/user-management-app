var React = require('react');
var ReactFire = require('reactfire');
var Firebase = require('firebase');
var LoginBox = require('./login-box');
var AdminPanel = require('./admin-panel/admin-panel');
var utilities = require('./utilities');
var dbUrl = 'https://anapp.firebaseio.com/';
var appInitialState = {
      adminName: '',
      adminPassword: '',
      rememberMe: '',
      
      userName: '',
      userLastName: '',
      userEmail: '',
      userGroup: 'No group',
      
      userLoggedIn: window.localStorage.rememberAdmin || false,
      errorMsg: false,
      users: {},
      groups: {},
      
      menuOpen: false
    };

module.exports = React.createClass({
  mixins: [ ReactFire, utilities ],
  getInitialState: function() {
    return appInitialState;
  },
  componentWillMount: function() {
    this.rootDB = new Firebase(dbUrl);
    
    this.usersData = new Firebase(dbUrl + 'users/');
    this.bindAsObject(this.usersData, 'users');
    
    this.groupsData = new Firebase(dbUrl + 'groups/');
    this.bindAsObject(this.groupsData, 'groups');
    
    this.rootDB.on('value', this.handleDataLoaded);
  },
  updateInputValue: function(value, property) {
    this.setState({
      [property]: value
    });
  },
  render: function() {
    return this.state.userLoggedIn 
      
      ? 
      
      <AdminPanel 
        users={this.state.users}
        groups={this.state.groups}
        dataLoaded={this.state.dataLoaded}
        menuOpen={this.state.menuOpen}
        updateInputValue={this.updateInputValue}
        addUser={this.addUser}
        removeUser={this.removeUser}       
        addGroup={this.addGroup}
        toggleMenu={this.toggleMenu}       
      /> 
    
      :
      
      <LoginBox 
        adminName={this.state.adminName}
        adminPassword={this.state.adminPassword}
        rememberMe={this.state.rememberMe}
        errorMsg={this.state.errorMsg}
        loginHandler={this.loginHanlder}
        updateInputValue={this.updateInputValue}
      /> 
  }
});