module.exports = {
  loginHanlder: function() {
    this.rootDB.authWithPassword({
      email: this.state.adminName,
      password: this.state.adminPassword
    }, this.loginCallback);
  },
  loginCallback: function(error, authData) {
    if (error) {
      
      this.setState({errorMsg: true});
      
    } else {
      
      this.setState({userLoggedIn: true});
      
      if (this.state.rememberMe) {
        window.localStorage.setItem('rememberAdmin', true);
      }
    }
  },
  addUser: function() {
    this.usersData.push({
      userName: this.state.userName,
      userLastName: this.state.userLastName,
      userEmail: this.state.userEmail,
      userGroup: this.state.userGroup
    });
  },
  removeUser: function(userID) {
    this.usersData.child(userID).remove();
  },
  addGroup: function() {
    this.groupsData.push({
      name: this.state.groupName,
    });
  },
  handleDataLoaded: function(){
    this.setState({
      dataLoaded: true
    });
  },
  toggleMenu: function() {
    this.setState({
      menuOpen: !this.state.menuOpen
    });
  }
}