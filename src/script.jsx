;(function(){

  var React = require('react');
  var ReactDOM = require('react-dom');
  var App = require('./app');
  
  ReactDOM.render(React.createElement(App), document.querySelector('.page-container'));

})();