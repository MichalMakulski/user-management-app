var React = require('react');

module.exports = React.createClass({
  render: function() { 
    return <div className="login-box-container">
        <div className="login-box">
          <h1>LOGIN TO ADMIN PANEL</h1>
          <input
            id="adminName"
            type="text"
            placeholder="Admin e-mail"
            value={this.props.adminName} 
            onChange={this.updateInputValue} 
          />
          <input 
            id="adminPassword"
            type="password"
            placeholder="Password"
            value={this.props.adminPassword} 
            onChange={this.updateInputValue} 
          />
          <div className="remeberMe-container">
            <input onChange={this.updateInputValue} type="checkbox" id="rememberMe" value={this.props.rememberMe} /> 
            <label for="rememberMe">Remember me</label>
          </div>
          <button onClick={this.props.loginHandler} type="button">LOGIN</button>
          <p className="error-msg">{this.props.errorMsg ? 'Something went wrong...' : ''}</p>
        </div>
    </div>
  },
  updateInputValue: function(ev) {
    var target = ev.target;
    var property = target.id;
    var value;
    
    target.type === 'checkbox' ? value = target.checked : value = target.value;
    
    this.props.updateInputValue(value, property);
  }
});