var React = require('react');
var GroupsList = require('./groups-list');

module.exports = React.createClass({
  render: function() { 
    return <div className="user-form">
        <input
          id="userName"
          type="text"
          placeholder="User name"
          value={this.props.userName} 
          onChange={this.updateInputValue} 
        />
        <input 
          id="userLastName"
          type="text"
          placeholder="User last name"
          value={this.props.userLastName} 
          onChange={this.updateInputValue} 
        />
        <input 
          id="userEmail"
          type="email"
          placeholder="User e-mail"
          value={this.props.userEmail} 
          onChange={this.updateInputValue} 
        />
        <GroupsList groups={this.props.groups} updateInputValue={this.updateInputValue} />
        <button onClick={this.props.addUser} type="button">Add user</button>
      </div>
  },
  updateInputValue: function(ev) {
    var text = ev.target.value;
    var property = ev.target.id;
    this.props.updateInputValue(text, property);
  }
});