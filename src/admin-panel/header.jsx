var React = require('react');

module.exports = React.createClass({
  render: function() { 
    return <div className="header">
        <h1>Admin Panel</h1>
        <div className="menu-container">
          <div className="main-menu">
            <i onClick={this.props.toggleMenu} className="fa fa-user-plus fa-2x" aria-hidden="true"></i>
          </div>
        </div>  
      </div>
  }
});