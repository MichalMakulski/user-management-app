var React = require('react');
var CreateForm = require('./create-form');
var CreateGroupForm = require('./create-group-form');

module.exports = React.createClass({
  render: function() { 
    return <div className="admin-panel-forms"> 
      <CreateGroupForm
        updateInputValue={this.props.updateInputValue}
        addGroup={this.props.addGroup}
      />
      <CreateForm
        updateInputValue={this.props.updateInputValue}
        addUser={this.props.addUser}
        groups={this.props.groups}  
      />  
    </div>  
  }
});