var React = require('react');

module.exports = React.createClass({
  render: function() { 
    return <div className="group-form">
        <input
          id="groupName"
          type="text"
          placeholder="Group name"
          value={this.props.groupName} 
          onChange={this.updateInputValue} 
        />
        <button onClick={this.props.addGroup} type="button">Create group</button>
      </div>
  },
  updateInputValue: function(ev) {
    var text = ev.target.value;
    var property = ev.target.id;
    this.props.updateInputValue(text, property);
  }
});