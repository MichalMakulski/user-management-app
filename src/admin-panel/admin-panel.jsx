var React = require('react');
var Header = require('./header');
var UsersList = require('./users-list');
var Forms = require('./forms');


module.exports = React.createClass({
  render: function() { 
    return <div className={"admin-panel-container" + (this.props.menuOpen ? ' menu-open' : '')} > 
      <Header toggleMenu={this.props.toggleMenu} />
      <UsersList 
        dataLoaded={this.props.dataLoaded}
        removeUser={this.props.removeUser} 
        users={this.props.users} 
      />
      <Forms
        updateInputValue={this.props.updateInputValue}
        addGroup={this.props.addGroup}
        addUser={this.props.addUser}
        groups={this.props.groups}
      />
    </div>  
  }
});