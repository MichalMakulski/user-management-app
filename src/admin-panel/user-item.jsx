var React = require('react');

module.exports = React.createClass({
  
  render: function() {
    return <div className="user-item">
      <div className="user-item__name"> {this.props.name} </div>
      <div className="user-item__last-name"> {this.props.lastName} </div>
      <div className="user-item__email"> {this.props.email} </div>
      <div className="user-item__group"> {this.props.group} </div>
      <div className="user-item__remove" onClick={this.props.removeUser.bind(null, this.props.uid)}> 
        <i className="fa fa-trash-o" aria-hidden="true"></i>
      </div>
    </div>
  } 
  
});