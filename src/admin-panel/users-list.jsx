var React = require('react');
var UserItem = require('./user-item');

module.exports = React.createClass({
  render: function() {
    var isDataLoaded = this.props.dataLoaded;
    
    return isDataLoaded 
      
    ? 
    
    this.renderUserList()
      
    :
    
    <div className="loader-container">
      <i className="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i>  
    </div>  
    
  },
  getUsers: function() {
    var children = [];
    for(var key in this.props.users) {
      var user = this.props.users[key];
      user.uid = key;
      children.push(user);
    }
    return children;
  },
  processUserData: function(user, idx) {
    return <UserItem 
      key={user.uid} 
      uid={user.uid} 
      name={user.userName} 
      lastName={user.userLastName} 
      email={user.userEmail}
      group={user.userGroup}
      removeUser={this.props.removeUser}       
    />
  },
  renderUserList: function() {
    var content = this.getUsers();
   
    if(!content.length) {
      
      return <div className="users-list-container">
      <h2>Add your first user</h2>
    </div>
      
    }else {
      var users = content.map(this.processUserData);
      
      return <div className="users-list-container">
          { users }
      </div>
      
    }
  }
  
});