var React = require('react');
var GroupItem = require('./group-item');

module.exports = React.createClass({
  render: function() {
    var groups = this.getGroups().map(this.processGroupData);
      return <select id="userGroup" onChange={this.props.updateInputValue}>
      <option value="volvo">No group</option>
      {groups}
    </select>
  },
  getGroups: function() {
    var children = [];
    for(var key in this.props.groups) {
      var group = this.props.groups[key];
      group.uid = key;
      children.push(group);
    }
    return children;
  },
  processGroupData: function(group, idx) {
    return <GroupItem 
      key={group.uid} 
      name={group.name} 
    />
  }
  
});